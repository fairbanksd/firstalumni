FIRST Alumni Website

Until we get a domain name, I'll try to keep the website at http://first.valkoria.com up to date with the latest changes in the repos.   Go here if you want to check out the progress!

This is an open project for the FIRST Alumni website which will be launching soon.  

It is a collaborative project, and all alum are welcome to contribute.  
We're always open to ideas!  For the sake of a timely launch, we ask that all collaboration run through Lea Fairbanks darunada@gmail.com

This is intended to be an open community for information gathering, communication, and alumni organization in the FIRST community.  And more: if you have an idea please share it!

WE LOVE FIRST!  ROBOTS RULE! WOOHOO!
